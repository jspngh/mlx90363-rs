use crate::Error;

// TODO: convert to hex
const EEPROM_PROTECTION_KEYS: [u16; 32] = [
    0x444D, 0x794D, 0xDF66, 0xE17C, 0x1EDB, 0xD127, 0x688B, 0x30F0, 0x94D9, 0xC866, 0x3F51, 0x610F,
    0x334E, 0xCC73, 0x38C2, 0x47AE, 0xD954, 0xFBDD, 0x9FC9, 0xB1BA, 0x5F5B, 0x8F45, 0x1075, 0xBECB,
    0x18E0, 0x1713, 0x7A98, 0xF75D, 0x0DEA, 0x4D68, 0x1B53, 0x0C4B,
];

pub fn get_key_for_address(address: u8) -> u16 {
    EEPROM_PROTECTION_KEYS[((address & 0b00111110) >> 1) as usize]
}

#[derive(Debug)]
pub enum EepromCode {
    /// EEPROM write success
    Success,
    /// Failed to erase or write the EEPROM
    WriteFail,
    /// Failed to erase or write the EEPROM CRC
    CrcWriteFail,
    /// Used invalid protection key
    InvalidKey,
    /// Incorrect challenge response
    ChallengeFail,
    /// Tried to write invalid address
    InvalidAddress,
}

impl EepromCode {
    const EE_CODE_SUCCESS: u8 = 1;
    const EE_CODE_FAIL: u8 = 2;
    const EE_CODE_CRC_FAIL: u8 = 4;
    const EE_CODE_KEY_INVALID: u8 = 6;
    const EE_CODE_CHALLENGE_FAIL: u8 = 7;
    const EE_CODE_ODD_ADDRESS: u8 = 8;

    pub fn from_bits<E>(bits: u8) -> Result<Self, Error<E>> {
        match bits {
            Self::EE_CODE_SUCCESS => Ok(Self::Success),
            Self::EE_CODE_FAIL => Ok(Self::WriteFail),
            Self::EE_CODE_CRC_FAIL => Ok(Self::CrcWriteFail),
            Self::EE_CODE_KEY_INVALID => Ok(Self::InvalidKey),
            Self::EE_CODE_CHALLENGE_FAIL => Ok(Self::ChallengeFail),
            Self::EE_CODE_ODD_ADDRESS => Ok(Self::InvalidAddress),
            _ => Err(Error::<E>::IncorrectData),
        }
    }
}
