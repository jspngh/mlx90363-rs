#[derive(Debug)]
pub enum DiagnosticStatus {
    /// First Diagnostics Sequence Not Yet Finished
    FirstSeqNotFinished,
    /// Diagnostic Fail
    Fail,
    /// Diagnostic Pass (previous cycle)
    PassPrevious,
    /// Diagnostic Pass – new cycle completed
    PassNew,
}

impl DiagnosticStatus {
    pub fn from_bits(bits: u8) -> Self {
        match bits {
            0b00 => Self::FirstSeqNotFinished,
            0b10 => Self::PassPrevious,
            0b11 => Self::PassNew,
            _ => Self::Fail,
        }
    }
}

/// Details not available in datasheet
pub enum ErrorInterruption {
    ProtectionErrorInterruption,
    InvalidAddressErrorInterruption,
    ProgramErrorInterruption,
    ExchangeErrorInterruption,
    NotConnectedErrorInterruption,
    StackInterrupt,
    FlowControlError,
}

#[derive(Debug)]
/// Fail-safe mode status
pub enum FailSafeModeStatus {
    NotInFSM,
    SelfTestError,
    DigitalDiagnosticError,
    ErrorInterruptionOccurred,
}

#[derive(Debug)]
pub struct Diagnostics {
    /// Error in RAM March C-10N test (at startup)
    ram_march_test: bool,
    /// Error in watchdog built-in self test (at startup)
    watchdog_bist: bool,
    /// ROM does not match its 16-bit checksum
    rom_checksum: bool,
    /// Error during continuous RAM test
    ram_test: bool,
    /// Error in CPU register functional test
    cpu_register_functional_test: bool,
    /// EEPROM calibration parameters do not match the 8-bit CRC
    eeprom_calibration_params: bool,
    /// EEPROM error detected
    eeprom_hamming_code_ded: bool,
    /// EEPROM RAM cache error
    eeprom_ram_cache_error: bool,
    adc_block: bool,
    bz_sensitivity_monitor: bool,
    bx_sensitivity_monitor: bool,
    by_sensitivity_monitor: bool,
    temperature_sensor_monitor: bool,
    /// T > 190C or T < -80C
    temperature_out_of_range: bool,
    field_magnitude_too_high: bool,
    field_magnitude_too_low: bool,
    adc_clipping: bool,
    supply_voltage_monitor: bool,

    analog_diagnostics_counter: u8,
    /// root cause of fail safe mode entry
    fsme_rc: FailSafeModeStatus,
}

impl Diagnostics {
    pub fn from_bits(_bits: &[u8]) -> Self {
        // TODO
        Self {
            ram_march_test: false,
            watchdog_bist: false,
            rom_checksum: false,
            ram_test: false,
            cpu_register_functional_test: false,
            eeprom_calibration_params: false,
            eeprom_hamming_code_ded: false,
            eeprom_ram_cache_error: false,
            adc_block: false,
            bz_sensitivity_monitor: false,
            bx_sensitivity_monitor: false,
            by_sensitivity_monitor: false,
            temperature_sensor_monitor: false,
            temperature_out_of_range: false,
            field_magnitude_too_high: false,
            field_magnitude_too_low: false,
            adc_clipping: false,
            supply_voltage_monitor: false,
            analog_diagnostics_counter: 0,
            fsme_rc: FailSafeModeStatus::NotInFSM,
        }
    }
}
