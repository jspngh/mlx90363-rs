use crate::crc8;
use crate::diagnostics;
use crate::eeprom;
use crate::Error;

use core::cmp::PartialEq;

pub type Message = [u8; 8];

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Marker {
    Alpha = 0b00000000,
    AlphaBeta = 0b01000000,
    XYZ = 0b10000000,
    Other = 0b11000000,
}

#[derive(Debug)]
pub struct GetData {
    /// defines the regular data message type
    pub marker: Marker,
    /// maximum lifetime of the regular data message, in microseconds
    pub timeout: u16,
    /// if the rolling counter should be reset
    pub reset: bool,
}

#[derive(Debug)]
pub struct MemoryReadData {
    // first address to read
    pub addr0: u16,
    /// second address to read
    pub addr1: u16,
}

#[derive(Debug)]
pub struct EepromWriteData {
    /// in range [0..0x3e], and should be even
    pub address: u8,
    pub data: u16,
}

#[derive(Debug)]
pub struct EepromChallengeAnswerData {
    pub key_echo: u16,
    pub inverted_key_echo: u16,
}

#[derive(Debug)]
pub struct NOPChallengeData {
    pub key: u16,
}

/// Command message, sent by the controller to the sensor
#[derive(Debug)]
pub enum Command {
    /// Get Regular message - Trigger Mode 1
    Get1(GetData),
    /// Get Regular message - Trigger Mode 2
    Get2(GetData),
    /// Get Regular message - Trigger Mode 3
    Get3(GetData),
    /// Read data from RAM (0x0000..0x00FE), EEPROM (0x1000..0x130E) or ROM (0x4000..0x5FFE)
    MemoryRead(MemoryReadData),
    /// Issue a write request to the EEPROM
    EepromWrite(EepromWriteData),
    /// Request the EEPROM challenge after the write operation
    EepromChallengeRead,
    /// Answer the EEPROM challenge
    EepromChallengeAnswer(EepromChallengeAnswerData),
    /// *Empty* command used for receiving data
    NOPChallenge(NOPChallengeData),
    /// Get detailed analysis of the diagnostics
    DiagnosticDetails,
    /// Enable software counter to evaluate internal oscillator frequency
    OscCounterStart,
    /// Stop the counter and return the value
    OscCounterStop,
    /// System reset identical to POR, valid after EEPROM write, in fail-safe mode or in standby mode
    Reboot,
    /// Set the sensor in standby mode, valid after NOP or DiagnosticDetails
    Standby,
}

impl Command {
    const OPCODE_GET1: u8 = 0x13;
    const OPCODE_GET2: u8 = 0x14;
    const OPCODE_GET3: u8 = 0x15;
    const OPCODE_MEMORYREAD: u8 = 0x01;
    const OPCODE_EEWRITE: u8 = 0x03;
    const OPCODE_EECHALLENGEANS: u8 = 0x05;
    const OPCODE_EEREADCHALLENGE: u8 = 0x0F;
    const OPCODE_NOP: u8 = 0x10;
    const OPCODE_DIAGNOSTICDETAILS: u8 = 0x16;
    const OPCODE_OSCCOUNTERSTART: u8 = 0x18;
    const OPCODE_OSCCOUNTERSTOP: u8 = 0x1A;
    const OPCODE_REBOOT: u8 = 0x2F;
    const OPCODE_STANDBY: u8 = 0x31;

    pub fn to_message(&self) -> Message {
        let mut msg = [0u8; 8];
        match self {
            Self::Get1(data) => {
                msg[1] = if data.reset { 1 } else { 0 };
                msg[2..4].copy_from_slice(&data.timeout.to_le_bytes());
                msg[6] = Self::OPCODE_GET1 | data.marker as u8;
            }
            Self::Get2(data) => {
                msg[1] = if data.reset { 1 } else { 0 };
                msg[2..4].copy_from_slice(&data.timeout.to_le_bytes());
                msg[6] = Self::OPCODE_GET2 | data.marker as u8;
            }
            Self::Get3(data) => {
                msg[1] = if data.reset { 1 } else { 0 };
                msg[2..4].copy_from_slice(&data.timeout.to_le_bytes());
                msg[6] = Self::OPCODE_GET3 | data.marker as u8;
            }
            Self::MemoryRead(data) => {
                msg[0..2].copy_from_slice(&data.addr0.to_le_bytes());
                msg[2..4].copy_from_slice(&data.addr1.to_le_bytes());
                msg[6] = Self::OPCODE_MEMORYREAD | Marker::Other as u8;
            }
            Self::EepromWrite(data) => {
                msg[1] = data.address;
                let key = eeprom::get_key_for_address(data.address);
                msg[2..4].copy_from_slice(&key.to_le_bytes());
                msg[4..6].copy_from_slice(&data.data.to_le_bytes());
                msg[6] = Self::OPCODE_EEWRITE | Marker::Other as u8;
            }
            Self::EepromChallengeRead => {
                msg[6] = Self::OPCODE_EEREADCHALLENGE | Marker::Other as u8;
            }
            Self::EepromChallengeAnswer(data) => {
                msg[2..4].copy_from_slice(&data.key_echo.to_le_bytes());
                msg[4..6].copy_from_slice(&data.inverted_key_echo.to_le_bytes());
                msg[6] = Self::OPCODE_EECHALLENGEANS | Marker::Other as u8;
            }
            Self::NOPChallenge(data) => {
                msg[2..4].copy_from_slice(&data.key.to_le_bytes());
                msg[6] = Self::OPCODE_NOP | Marker::Other as u8;
            }
            Self::DiagnosticDetails => {
                msg[6] = Self::OPCODE_DIAGNOSTICDETAILS | Marker::Other as u8;
            }
            Self::OscCounterStart => {
                msg[6] = Self::OPCODE_OSCCOUNTERSTART | Marker::Other as u8;
            }
            Self::OscCounterStop => {
                msg[6] = Self::OPCODE_OSCCOUNTERSTOP | Marker::Other as u8;
            }
            Self::Reboot => {
                msg[6] = Self::OPCODE_REBOOT | Marker::Other as u8;
            }
            Self::Standby => {
                msg[6] = Self::OPCODE_STANDBY | Marker::Other as u8;
            }
        }
        crc8::fill_crc(&msg)
    }
}

#[derive(Debug)]
pub struct AlphaMessage {
    pub alpha: u16,
    pub diagnostics: diagnostics::DiagnosticStatus,
    pub vg: u8,
    /// roll counter
    pub roll: u8,
}

#[derive(Debug)]
pub struct AlphaBetaMessage {
    pub alpha: u16,
    pub beta: u16,
    pub diagnostics: diagnostics::DiagnosticStatus,
    pub vg: u8,
    /// roll counter
    pub roll: u8,
}

#[derive(Debug)]
pub struct XYZMessage {
    pub x: u16,
    pub y: u16,
    pub z: u16,
    pub diagnostics: diagnostics::DiagnosticStatus,
    /// roll counter
    pub roll: u8,
}

#[derive(Debug)]
pub enum RegularMessageData {
    Alpha(AlphaMessage),
    AlphaBeta(AlphaBetaMessage),
    XYZ(XYZMessage),
}

impl RegularMessageData {
    fn parse(data: &[u8], marker: Marker, roll: u8) -> Self {
        match marker {
            Marker::Alpha => Self::Alpha(AlphaMessage {
                alpha: u16::from_le_bytes([data[0], data[1]]) & 0x3FFF,
                diagnostics: diagnostics::DiagnosticStatus::from_bits((data[1] & 0b11000000) >> 6),
                vg: data[4],
                roll,
            }),
            Marker::AlphaBeta => Self::AlphaBeta(AlphaBetaMessage {
                alpha: u16::from_le_bytes([data[0], data[1]]) & 0x3FFF,
                beta: u16::from_le_bytes([data[2], data[3]]) & 0x3FFF,
                diagnostics: diagnostics::DiagnosticStatus::from_bits((data[1] & 0b11000000) >> 6),
                vg: data[4],
                roll,
            }),
            Marker::XYZ => Self::XYZ(XYZMessage {
                x: u16::from_le_bytes([data[0], data[1]]) & 0x3FFF,
                y: u16::from_le_bytes([data[2], data[3]]) & 0x3FFF,
                z: u16::from_le_bytes([data[4], data[5]]) & 0x3FFF,
                diagnostics: diagnostics::DiagnosticStatus::from_bits((data[1] & 0b11000000) >> 6),
                roll,
            }),
            // since this is not a public function,
            // we know that `Marker::Other` will never be passed here
            _ => unreachable!(),
        }
    }
}

#[derive(Debug)]
pub struct MemoryReadAnswerData {
    pub addr0_data: u16,
    pub addr1_data: u16,
}

#[derive(Debug)]
pub struct EepromChallengeData {
    pub challenge_key: u16,
}

#[derive(Debug)]
pub struct EepromWriteStatusData {
    pub code: eeprom::EepromCode,
}

#[derive(Debug)]
pub struct NOPResponseData {
    pub key_echo: u16,
    pub inverted_key_echo: u16,
}

#[derive(Debug)]
pub struct OscCounterData {
    pub counter_value: u16,
}

#[derive(Debug)]
pub struct ReadyData {
    pub hw_version: u8,
    pub fw_version: u8,
}

#[derive(Debug)]
pub enum ErrorCode {
    /// MOSI message size != 64 bits
    IncorrectBitCount,
    /// MOSI message had an invalid CRC
    IncorrectCRC,
    /// Answer Time-Out or Answer not Ready
    NothingToTransmit, // why is this also an error-code?
    /// MOSI message had an invalid opcode
    OpcodeInvalid,
}

impl ErrorCode {
    fn from_bits<E>(bits: u8) -> Result<Self, Error<E>> {
        match bits {
            1 => Ok(Self::IncorrectBitCount),
            2 => Ok(Self::IncorrectCRC),
            3 => Ok(Self::NothingToTransmit),
            4 => Ok(Self::OpcodeInvalid),
            _ => Err(Error::<E>::IncorrectData),
        }
    }
}

#[derive(Debug)]
pub struct ErrorData {
    pub error_code: ErrorCode,
}

/// Response data coming from the sensor
#[derive(Debug)]
pub enum Response {
    RegularMessage(RegularMessageData),
    Get3Ready,
    MemoryReadAnswer(MemoryReadAnswerData),
    /// 16-bit challenge key that should be echoed to allow EEPROM write
    EepromChallenge(EepromChallengeData),
    /// *Filler* message for the EEPROM write sequence
    EepromReadAnswer,
    /// Result of the EEPROM write sequence
    EepromWriteStatus(EepromWriteStatusData),
    NOPResponse(NOPResponseData),
    DiagnosticAnswer(diagnostics::Diagnostics),
    OscCounterStartAck,
    OscCounterStopAck(OscCounterData),
    StandbyAck,
    ErrorFrame(ErrorData),
    NothingToTransmit,
    Ready(ReadyData),
}

impl Response {
    const OPCODE_GET3READY: u8 = 0x2D;
    const OPCODE_MEMORYREADANSWER: u8 = 0x02;
    const OPCODE_EEWRITECHALLENGE: u8 = 0x04;
    const OPCODE_EEREADANSWER: u8 = 0x28;
    const OPCODE_EEWRITESTATUS: u8 = 0x0E;
    const OPCODE_NOP: u8 = 0x11;
    const OPCODE_DIAGNOSTICANSWER: u8 = 0x17;
    const OPCODE_OSCCOUNTERSTARTACK: u8 = 0x19;
    const OPCODE_OSCCOUNTERSTOPACK: u8 = 0x1B;
    const OPCODE_STANDBYACK: u8 = 0x32;
    const OPCODE_ERRORFRAME: u8 = 0x3D;
    const OPCODE_NTT: u8 = 0x3E;
    const OPCODE_READY: u8 = 0x2C;

    pub fn from_message<E>(msg: &[u8]) -> Result<Self, Error<E>> {
        if msg.len() != 8 {
            return Err(Error::IncorrectSlaveBitCount);
        }
        if !crc8::check_crc(msg) {
            return Err(Error::IncorrectSlaveCRC);
        }
        let marker = msg[6] & 0b11000000;
        let opcode = msg[6] & 0b00111111;

        let marker = match marker {
            0b00000000 => Marker::Alpha,
            0b01000000 => Marker::AlphaBeta,
            0b10000000 => Marker::XYZ,
            _ => Marker::Other,
        };
        if marker != Marker::Other {
            // in this case, the value in `opcode` is the roll counter
            Ok(Self::RegularMessage(RegularMessageData::parse(
                &msg[0..6],
                marker,
                opcode,
            )))
        } else {
            match opcode {
                Self::OPCODE_GET3READY => Ok(Self::Get3Ready),
                Self::OPCODE_MEMORYREADANSWER => Ok(Self::MemoryReadAnswer(MemoryReadAnswerData {
                    addr0_data: u16::from_le_bytes([msg[0], msg[1]]),
                    addr1_data: u16::from_le_bytes([msg[2], msg[3]]),
                })),
                Self::OPCODE_EEWRITECHALLENGE => Ok(Self::EepromChallenge(EepromChallengeData {
                    challenge_key: u16::from_le_bytes([msg[2], msg[3]]),
                })),
                Self::OPCODE_EEREADANSWER => Ok(Self::EepromReadAnswer),
                Self::OPCODE_EEWRITESTATUS => Ok(Self::EepromWriteStatus(EepromWriteStatusData {
                    code: eeprom::EepromCode::from_bits(msg[0])?,
                })),
                Self::OPCODE_NOP => Ok(Self::NOPResponse(NOPResponseData {
                    key_echo: u16::from_le_bytes([msg[2], msg[3]]),
                    inverted_key_echo: u16::from_le_bytes([msg[4], msg[5]]),
                })),
                Self::OPCODE_DIAGNOSTICANSWER => Ok(Self::DiagnosticAnswer(
                    diagnostics::Diagnostics::from_bits(&msg[0..6]),
                )),
                Self::OPCODE_OSCCOUNTERSTARTACK => Ok(Self::OscCounterStartAck),
                Self::OPCODE_OSCCOUNTERSTOPACK => Ok(Self::OscCounterStopAck(OscCounterData {
                    counter_value: u16::from_le_bytes([msg[2], msg[3]]) & 0x7FFF,
                })),
                Self::OPCODE_STANDBYACK => Ok(Self::StandbyAck),
                Self::OPCODE_ERRORFRAME => Ok(Self::ErrorFrame(ErrorData {
                    error_code: ErrorCode::from_bits(msg[0])?,
                })),
                Self::OPCODE_NTT => Ok(Self::NothingToTransmit),
                Self::OPCODE_READY => Ok(Self::Ready(ReadyData {
                    hw_version: msg[0],
                    fw_version: msg[1],
                })),
                _ => Err(Error::<E>::IncorrectOpcode),
            }
        }
    }
}
