// make `std` available when testing
#![cfg_attr(not(test), no_std)]
#![no_main]

use embedded_hal as hal;
use hal::blocking::{delay, spi};
use hal::digital::v2::OutputPin;

pub mod crc8;
pub mod opcode;

mod diagnostics;
mod eeprom;

/// Errors
#[derive(Debug)]
pub enum Error<E> {
    /// MOSI message bit count != 64
    IncorrectMasterBitCount,
    /// MISO message bit count != 64
    IncorrectSlaveBitCount,
    /// MOSI message has a CRC error
    IncorrectMasterCRC,
    /// MISO message has a CRC error
    IncorrectSlaveCRC,
    /// Timeout or still processing the previous command
    Timeout,
    /// Opcode in response is invalid
    IncorrectOpcode,
    /// Data in response is not according to spec
    IncorrectData,
    /// SPI bus error
    Spi(E),
    /// GPIO error
    Gpio, // TODO: also give inner error?
    /// Invalid address parameter when reading or writing memory
    InvalidMemoryAddress,
}

pub struct Mlx90363<SPI, CS> {
    spi: SPI,
    cs: CS,
}

impl<E, SPI, CS> Mlx90363<SPI, CS>
where
    SPI: spi::Transfer<u8, Error = E> + spi::Write<u8, Error = E>,
    CS: OutputPin,
{
    pub fn new(spi: SPI, chip_select: CS) -> Result<Self, E> {
        let mlx90363 = Mlx90363 {
            spi,
            cs: chip_select,
        };
        Ok(mlx90363)
    }

    pub fn nop(&mut self, challenge: u16) -> Result<opcode::Response, Error<E>> {
        let nop = opcode::Command::NOPChallenge(opcode::NOPChallengeData { key: challenge });
        let mut nop = nop.to_message();

        let response = self.transfer(&mut nop)?;
        opcode::Response::from_message(response)
    }

    pub fn get1(
        &mut self,
        marker: opcode::Marker,
        timeout: u16,
        reset: bool,
    ) -> Result<opcode::Response, Error<E>> {
        let get1 = opcode::Command::Get1(opcode::GetData {
            marker,
            timeout,
            reset,
        });
        let mut get1 = get1.to_message();

        let response = self.transfer(&mut get1)?;
        opcode::Response::from_message(response)
    }

    pub fn get2(
        &mut self,
        marker: opcode::Marker,
        timeout: u16,
        reset: bool,
    ) -> Result<opcode::Response, Error<E>> {
        let get2 = opcode::Command::Get2(opcode::GetData {
            marker,
            timeout,
            reset,
        });
        let mut get2 = get2.to_message();

        let response = self.transfer(&mut get2)?;
        opcode::Response::from_message(response)
    }

    pub fn get3(
        &mut self,
        marker: opcode::Marker,
        timeout: u16,
        reset: bool,
    ) -> Result<opcode::Response, Error<E>> {
        let get3 = opcode::Command::Get3(opcode::GetData {
            marker,
            timeout,
            reset,
        });
        let mut get3 = get3.to_message();

        let response = self.transfer(&mut get3)?;
        opcode::Response::from_message(response)
    }

    /// Send a sync pulse on the /SS line
    /// Needs to be between 1 and 10 us
    pub fn sync<D>(&mut self, del: &mut D) -> Result<(), Error<E>>
    where
        D: delay::DelayUs<u8>,
    {
        self.cs.set_low().map_err(|_| Error::<E>::Gpio)?;
        del.delay_us(2);
        self.cs.set_high().map_err(|_| Error::<E>::Gpio)?;
        Ok(())
    }

    pub fn write_eeprom(&mut self) -> Result<(), Error<E>> {
        // TODO
        // EEWrite
        // EEReadChallenge
        // EEChallengeAns
        // NOP to get EEWriteStatus
        Ok(())
    }

    /// Read data from the RAM, EEPROM or ROM area, in one or more MemoryRead requests.
    /// This function will ignore any *leftover data* in the first response.
    /// The address buffer will be filled with the read data.
    ///
    /// RAM address space: [0x0000..0x00FE]
    /// EEPROM address space: [0x1000..0x103E]
    /// ROM address space: [0x4000..0x5FFE]
    pub fn read_memory<'w>(&mut self, addr: &'w mut [u16]) -> Result<&'w mut [u16], Error<E>> {
        // TODO: also check that addresses are even
        for a in addr.iter() {
            match a {
                0x0000..=0x00FE => (),
                0x1000..=0x103E => (),
                0x4000..=0x5FFE => (),
                _ => return Err(Error::InvalidMemoryAddress),
            }
        }
        self.read_memory_unchecked(addr)
    }

    /// Read data from memory, but do not check for invalid addresses
    pub fn read_memory_unchecked<'w>(
        &mut self,
        addr: &'w mut [u16],
    ) -> Result<&'w mut [u16], Error<E>> {
        // a MemoryRead request can fetch data from 2 addresses at the same time
        for i in (0..addr.len()).step_by(2) {
            let data = if i + 1 < addr.len() {
                opcode::MemoryReadData {
                    addr0: addr[i],
                    addr1: addr[i + 1],
                }
            } else {
                opcode::MemoryReadData {
                    addr0: addr[i],
                    addr1: 0,
                }
            };

            let mut read = opcode::Command::MemoryRead(data).to_message();
            let response = self.transfer(&mut read)?;
            if i > 0 {
                let response = opcode::Response::from_message(response)?;
                match response {
                    opcode::Response::MemoryReadAnswer(d) => {
                        addr[i - 2] = d.addr0_data;
                        addr[i - 1] = d.addr1_data;
                    }
                    _ => return Err(Error::IncorrectOpcode),
                }
            }
        }
        // get last data
        let response = self.nop(0x1234)?;
        match response {
            opcode::Response::MemoryReadAnswer(d) => {
                let l = addr.len();
                if (l % 2) == 0 {
                    addr[l - 2] = d.addr0_data;
                    addr[l - 1] = d.addr1_data;
                } else {
                    addr[l - 1] = d.addr0_data;
                }
            }
            _ => return Err(Error::IncorrectOpcode),
        }
        Ok(addr)
    }

    fn transfer<'w>(&mut self, words: &'w mut [u8]) -> Result<&'w [u8], Error<E>> {
        self.cs.set_low().map_err(|_| Error::<E>::Gpio)?;
        let response = self.spi.transfer(words).map_err(Error::Spi)?;
        self.cs.set_high().map_err(|_| Error::<E>::Gpio)?;
        Ok(response)
    }
}
