# MLX90363 Rust driver
This is a `no_std` driver for the MLX90363 magnetic sensor IC by Melexis.

This is still a work-in-progress, but should already be usable for simple use-cases.

## Examples
An example for the nRF52840 is available in the `examples/` directory.
More examples are planned to be added in the future, but it should be
fairly straightforward to use this library on any chip that
supports the [embedded-hal](https://crates.io/crates/embedded-hal) traits.

## License
Licensed under either of

- Apache License, Version 2.0 ([LICENSE-APACHE](LICENSE-APACHE) or
  http://www.apache.org/licenses/LICENSE-2.0)
- MIT license ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.

## Contribution
Unless you explicitly state otherwise, any contribution intentionally submitted for inclusion in the
work by you, as defined in the Apache-2.0 license, shall be dual licensed as above, without any
additional terms or conditions.
