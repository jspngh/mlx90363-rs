#![no_std]
#![no_main]

use defmt_rtt as _;
use nrf52840_hal as hal;
use panic_probe as _;

use hal::delay;
use hal::gpio::{self, Output, PushPull};
use hal::{spim, timer};

use mlx90363;

struct PinConfig {
    spi: spim::Pins,
    chip_select: gpio::p0::P0_02<Output<PushPull>>,
}

fn setup_pins(port0: gpio::p0::Parts, port1: gpio::p1::Parts) -> PinConfig {
    let spi_clk = port0
        .p0_08
        .into_push_pull_output(gpio::Level::Low)
        .degrade();
    let spi_mosi = port1
        .p1_09
        .into_push_pull_output(gpio::Level::Low)
        .degrade();
    let spi_miso = port0.p0_13.into_floating_input().degrade();
    let spi_cs = port0.p0_02.into_push_pull_output(gpio::Level::High);
    let spi = spim::Pins {
        sck: spi_clk,
        miso: Some(spi_miso),
        mosi: Some(spi_mosi),
    };

    PinConfig {
        spi,
        chip_select: spi_cs,
    }
}

#[cortex_m_rt::entry]
fn main() -> ! {
    let dp = hal::pac::Peripherals::take().unwrap();
    let cp = hal::pac::CorePeripherals::take().unwrap();

    let p0 = gpio::p0::Parts::new(dp.P0);
    let p1 = gpio::p1::Parts::new(dp.P1);
    let pin_config = setup_pins(p0, p1);

    let spi = spim::Spim::new(
        dp.SPIM0,
        pin_config.spi,
        spim::Frequency::K125,
        spim::MODE_1,
        0,
    );
    let spi_cs = pin_config.chip_select;
    let mut magneto = mlx90363::Mlx90363::new(spi, spi_cs).unwrap();

    let mut timer = timer::Timer::new(dp.TIMER0);
    let mut delay = delay::Delay::new(cp.SYST);

    defmt::info!("Waiting for startup");

    // wait for startup
    timer.delay(1_000_000u32);

    if let Ok(res) = magneto.nop(0xdead) {
        defmt::info!("{:?}", defmt::Debug2Format(&res));
    } else {
        defmt::error!("NOP message failed");
    }

    let mut cnt = 0;

    /* GET1 loop example
    loop {
        timer.delay(5000u32);
        if let Ok(res) = magneto.get1(mlx90363::opcode::Marker::Alpha, 10000, false) {
            if (cnt % 40) == 0 {
                defmt::info!("{:?}", defmt::Debug2Format(&res));
            }
            cnt += 1;
        } else {
            defmt::error!("GET message failed");
        }
    }
    */

    /* GET2 loop example
    loop {
        timer.delay(4000u32);
        if let Ok(res) = magneto.get2(mlx90363::opcode::Marker::AlphaBeta, 10000, false) {
            if (cnt % 40) == 0 {
                defmt::info!("{:?}", defmt::Debug2Format(&res));
            }
            cnt += 1;
            timer.delay(1000u32);
            magneto.sync(&mut delay).ok();
        } else {
            defmt::error!("GET message failed");
        }
    }
    */

    //* Memory read example
    let mut buf = [0x103A, 0x103C]; // USERID, should return [1, 3]
    if let Ok(res) = magneto.read_memory(&mut buf) {
        defmt::info!("{=[?]}", res);
    } else {
        defmt::error!("MemRead message failed");
    }
    exit()
    //*/
}

#[defmt::panic_handler]
fn panic() -> ! {
    cortex_m::asm::udf()
}

/// Terminates the application and makes `probe-run` exit with exit-code = 0
pub fn exit() -> ! {
    loop {
        cortex_m::asm::bkpt();
    }
}
